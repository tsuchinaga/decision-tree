package tree

import (
	"errors"
	"fmt"
)

type Pruner int

const (
	DepthPruner Pruner = iota
	ImpurityPruner
)

func (p Pruner) String() string {
	switch p {
	case DepthPruner:
		return "DepthPruner"
	case ImpurityPruner:
		return "ImpurityPruner"
	}
	return "Unknown Pruner"
}

func (p Pruner) GetFunction() func(*Tree, float64) error {
	switch p {
	case DepthPruner:
		return depthPruner
	case ImpurityPruner:
		return impurityPruner
	}
	return depthPruner
}

type PrunerOption struct {
	Pruner    Pruner
	Reference float64
}

func (opt *PrunerOption) String() string {
	return fmt.Sprintf("PrunerOption{Pruner: %s, Reference: %f}", opt.Pruner, opt.Reference)
}

func NewPrunerOption(pruner Pruner, reference float64) *PrunerOption {
	return &PrunerOption{Pruner: pruner, Reference: reference}
}

var (
	PrunerErrorTreeNotFound = errors.New("木が指定されていません")
	PrunerErrorRootNotFound = errors.New("ルートが指定されていません")
)

// 深さ剪定機
func depthPruner(tree *Tree, reference float64) error {
	if tree == nil {
		return PrunerErrorTreeNotFound
	}
	if tree.Root == nil {
		return PrunerErrorRootNotFound
	}

	depthCut(tree.Root, 1, reference)
	return nil
}

// ノードを指定の深さで切る
func depthCut(node *Node, depth, reference float64) {
	if depth == reference {
		// 目標の深さなので子ノードを切る
		node.IsLeaf = true
		node.DispatchOption = nil
		node.LeftNode = nil
		node.RightNode = nil
	} else if depth < reference {
		if node.LeftNode != nil {
			depthCut(node.LeftNode, depth+1, reference)
		}
		if node.RightNode != nil {
			depthCut(node.RightNode, depth+1, reference)
		}
	}
}

// 不純度剪定機
func impurityPruner(tree *Tree, reference float64) error {
	if tree == nil {
		return PrunerErrorTreeNotFound
	}
	if tree.Root == nil {
		return PrunerErrorRootNotFound
	}

	impurityCut(tree.Root, reference)
	return nil
}

// 不純度が基準未満になれば分割をやめる
func impurityCut(node *Node, reference float64) {
	if node.Impurity < reference { // 不純度が基準未満なので末端にする
		node.IsLeaf = true
		node.DispatchOption = nil
		node.LeftNode = nil
		node.RightNode = nil
	} else {
		if node.LeftNode != nil {
			impurityCut(node.LeftNode, reference)
		}
		if node.RightNode != nil {
			impurityCut(node.RightNode, reference)
		}
	}
}
