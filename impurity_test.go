package tree

import (
	"math"
	"testing"
)

func TestGini(t *testing.T) {
	testTable := []struct {
		data   []*DataItem
		expect float64
	}{
		{[]*DataItem{{Category: "0"}, {Category: "0"}}, 0},
		{[]*DataItem{{Category: "1"}, {Category: "1"}}, 0},
		{[]*DataItem{{Category: "0"}, {Category: "1"}}, 0.5},
		{[]*DataItem{{Category: "0"}, {Category: "1"}, {Category: "1"}, {Category: "1"}}, 0.375},
		{[]*DataItem{{Category: "0"}, {Category: "0"}, {Category: "0"}, {Category: "1"}}, 0.375},
		{[]*DataItem{{Category: "0"}, {Category: "1"}, {Category: "2"}, {Category: "3"}}, 0.75},
	}

	for i, test := range testTable {
		no := i + 1
		actual := gini(test.data)
		if test.expect != actual {
			t.Fatalf("%s No.%02d 失敗 期待: %f 実際: %f", t.Name(), no, test.expect, actual)
		}
	}
}

func TestEntropy(t *testing.T) {
	testTable := []struct {
		data   []*DataItem
		expect float64
	}{
		{[]*DataItem{{Category: "0"}, {Category: "0"}}, 0},
		{[]*DataItem{{Category: "1"}, {Category: "1"}}, 0},
		{[]*DataItem{{Category: "0"}, {Category: "1"}}, 1},
		{[]*DataItem{{Category: "0"}, {Category: "1"}, {Category: "1"}, {Category: "1"}}, 0.405639062229566416},
		{[]*DataItem{{Category: "0"}, {Category: "0"}, {Category: "0"}, {Category: "1"}}, 0.405639062229566416},
		{[]*DataItem{{Category: "0"}, {Category: "1"}, {Category: "2"}, {Category: "3"}}, 1},
	}

	for i, test := range testTable {
		no := i + 1
		actual := entropy(test.data)
		if test.expect != actual {
			t.Fatalf("%s No.%02d 失敗 期待: %f 実際: %f", t.Name(), no, test.expect, actual)
		}
	}
}

func TestImpurityIndex_GetFunction(t *testing.T) {
	testData := []*DataItem{
		{Category: "1"},
		{Category: "1"},
		{Category: "2"},
		{Category: "3"},
		{Category: "4"},
		{Category: "4"},
	}

	testTable := []struct {
		impurityIndex ImpurityIndex
		expectFunc    func([]*DataItem) float64
	}{
		{Gini, gini},
		{Entropy, entropy},
	}

	for i, test := range testTable {
		expect := math.Floor(test.expectFunc(testData)*10000000000) / 10000000000
		actual := math.Floor(test.impurityIndex.GetFunction()(testData)*10000000000) / 10000000000
		if expect != actual {
			t.Fatalf("%s No.%02d 失敗\n期待: %.16f, 実際: %.16f", t.Name(), i+1, expect, actual)
		}
	}
}

func TestImpurityIndex_String(t *testing.T) {
	testTable := []struct {
		impurityIndex ImpurityIndex
		expect        string
	}{
		{Gini, "Gini"},
		{Entropy, "Entropy"},
	}

	for i, test := range testTable {
		actual := test.impurityIndex.String()
		if test.expect != actual {
			t.Fatalf("%s No.%02d 失敗\n期待: %s, 実際: %s\n", t.Name(), i+1, test.expect, actual)
		}
	}
}
