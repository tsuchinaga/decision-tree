package tree

import (
	"errors"
	"fmt"
	"log"
	"math"
	"sort"
	"strconv"
	"strings"
)

// 左のノードか右のノードかそれ以外かってやつ
type Side int

const (
	OtherSide Side = iota // 左にも右にも振れない場合
	LeftSide              // 左
	RightSide             // 右
)

func (s Side) String() string {
	switch s {
	case OtherSide:
		return "OtherSide"
	case LeftSide:
		return "LeftSide"
	case RightSide:
		return "RightSide"
	}
	return "Unknown Side"
}

// datasetの名前などを保持した木を作る
func CreateTree(dataset *Dataset, impurityIndex ImpurityIndex, pruner *PrunerOption) *Tree {
	if dataset == nil {
		return nil
	}

	// 剪定なしの木を作成
	tree := &Tree{
		FeatureNames:  dataset.FeatureNames,
		CategoryNames: dataset.CategoryNames,
		ImpurityIndex: impurityIndex,
		Pruner:        pruner,
		Root:          createNode(dataset.Data, impurityIndex, 1),
	}

	// 剪定
	if pruner != nil {
		prunerFunc := pruner.Pruner.GetFunction()
		_ = prunerFunc(tree, pruner.Reference)
	}
	return tree
}

// 決定木
type Tree struct {
	FeatureNames  map[string]string // 特徴の名前
	CategoryNames map[string]string // 登場するカテゴリの名前
	ImpurityIndex ImpurityIndex     // 不純度の指標
	Pruner        *PrunerOption     // 剪定機
	Root          *Node             // ルートのノード
}

func (tree *Tree) String() string {
	var b strings.Builder

	b.WriteString("Tree {\n")
	b.WriteString(fmt.Sprintf("\tFeatureNames: %+v\n", tree.FeatureNames))
	b.WriteString(fmt.Sprintf("\tCategoryNames: %+v\n", tree.CategoryNames))
	b.WriteString(fmt.Sprintf("\tImpurityIndex: %s\n", tree.ImpurityIndex))
	b.WriteString(fmt.Sprintf("\tPruner: %s\n", tree.Pruner))
	b.WriteString(fmt.Sprintf("\tRoot: %s\n", tree.Root.String(2)))
	b.WriteString("}\n")

	return b.String()
}

// 引数のitemがどのノードに振り分けられるか、Tree内のNodeをたどっていく
func (tree *Tree) Judge(item *DataItem) *Node {
	node := tree.Root
	for !node.IsLeaf {
		switch dispatch(item, node.DispatchOption) {
		case LeftSide:
			node = node.LeftNode
		case RightSide:
			node = node.RightNode
		default:
			return node
		}
	}
	return node
}

// 剪定せずに限界まで枝を伸ばす
func createNode(data []*DataItem, impurityIndex ImpurityIndex, depth int) *Node {
	categoryCount := categoryCount(data)
	categoryTotal := 0
	categoryRatio := make(map[string]float64)
	for _, n := range categoryCount {
		categoryTotal += n
	}
	for i, n := range categoryCount {
		categoryRatio[i] = float64(n) / float64(categoryTotal)
	}

	impurityFunc := impurityIndex.GetFunction()
	opt, err := getDispatchOption(data, impurityIndex)
	isLeaf := false
	if err != nil { // 分割条件が取れないなら、分割はここで終わらせる
		isLeaf = true
	}

	node := &Node{
		Depth:          depth,
		IsLeaf:         isLeaf,
		CategoryTotal:  categoryTotal,
		CategoryCount:  categoryCount,
		CategoryRatio:  categoryRatio,
		Impurity:       impurityFunc(data),
		DispatchOption: opt,
	}

	// 分割できるなら、分割して子ノードを作る
	if !isLeaf {
		left := make([]*DataItem, 0)
		right := make([]*DataItem, 0)
		for _, item := range data {
			switch dispatch(item, opt) {
			case LeftSide:
				left = append(left, item)
			case RightSide:
				right = append(right, item)
			default:
				log.Printf("振り分けられないと判断されました。opt: %+v, item: %+v\n", opt, item)
			}
		}
		node.LeftNode = createNode(left, impurityIndex, depth+1)
		node.RightNode = createNode(right, impurityIndex, depth+1)
	}

	return node
}

// 決定木を構成するノード
type Node struct {
	Depth          int                // 深さ
	IsLeaf         bool               // 末端フラグ
	CategoryTotal  int                // カテゴリの各数の合計
	CategoryCount  map[string]int     // カテゴリ毎の数
	CategoryRatio  map[string]float64 // カテゴリ毎の割合
	Impurity       float64            // 不純度
	DispatchOption *DispatchOption    // 振り分け条件
	LeftNode       *Node              // 左の子ノード
	RightNode      *Node              // 右の子ノード
}

func (node *Node) String(indent int) string {
	var b strings.Builder

	b.WriteString("{\n")
	b.WriteString(fmt.Sprintf("%sDepth: %d\n", strings.Repeat("\t", indent), node.Depth))
	b.WriteString(fmt.Sprintf("%sIsLeaf: %s\n", strings.Repeat("\t", indent), strconv.FormatBool(node.IsLeaf)))
	b.WriteString(fmt.Sprintf("%sCategoryTotal: %d\n", strings.Repeat("\t", indent), node.CategoryTotal))
	b.WriteString(fmt.Sprintf("%sCategoryCount: %+v\n", strings.Repeat("\t", indent), node.CategoryCount))
	b.WriteString(fmt.Sprintf("%sCategoryRatio: %+v\n", strings.Repeat("\t", indent), node.CategoryRatio))
	b.WriteString(fmt.Sprintf("%sImpurity: %f\n", strings.Repeat("\t", indent), node.Impurity))
	if !node.IsLeaf {
		b.WriteString(fmt.Sprintf("%sDispatchOption: {FeatureIndex: %s, Pivot: %f}\n",
			strings.Repeat("\t", indent), node.DispatchOption.FeatureIndex, node.DispatchOption.Pivot))
		b.WriteString(fmt.Sprintf("%sLeftNode: %s\n", strings.Repeat("\t", indent), node.LeftNode.String(indent+1)))
		b.WriteString(fmt.Sprintf("%sRightNode: %s\n", strings.Repeat("\t", indent), node.RightNode.String(indent+1)))
	}
	b.WriteString(fmt.Sprintf("%s}", strings.Repeat("\t", indent-1)))

	return b.String()
}

// 末端ノードの数を数える
func (node *Node) CountLeaf() int {
	if node.IsLeaf {
		return 1
	}

	var l, r int
	if node.LeftNode != nil {
		l = node.LeftNode.CountLeaf()
	}
	if node.RightNode != nil {
		r = node.RightNode.CountLeaf()
	}
	return l + r
}

// データを右か左に振り分けるための設定
type DispatchOption struct {
	FeatureIndex string  // 振り分けに使う要素
	Pivot        float64 // 振り分けるボーダー 左はpivot未満、右はpivot以上
}

// GetDispatchOptionが返しうるエラー
var (
	getDispatchOptionErrorShortage = errors.New("dataは少なくとも2つ必要です")
	getDispatchOptionErrorMinimum  = errors.New("すでに不純度は最低で、これ以上分ける必要はありません")
	getDispatchOptionErrorUnable   = errors.New("最適な分け方がありません")
)

// dataをどう分けると不純度を最低にできるかって振り分けの条件を返す
func getDispatchOption(data []*DataItem, impurityIndex ImpurityIndex) (*DispatchOption, error) {
	// データが分けられなければエラー
	if len(data) <= 1 {
		return nil, getDispatchOptionErrorShortage
	}

	impurityFunc := impurityIndex.GetFunction()
	// 分ける前にすでに不純度が最低ならエラー
	currentImpurity := round(impurityFunc(data), -10) // 現在の不純度
	if currentImpurity == 0 {
		return nil, getDispatchOptionErrorMinimum
	}

	// 分け方を探索する処理
	var maxGain float64      // 最大の利得 少なくとも分割後は不純度が下がり利得が得られる必要がある
	var impurityDiff float64 // 最大の利得が出た時の最低の不純度差
	var opt *DispatchOption
	for featureIndex := range data[0].Features {
		// dataをfeatureIndex番目のFeaturesで昇順に並べる
		sort.Slice(data, func(i, j int) bool {
			return data[i].Features[featureIndex] < data[j].Features[featureIndex]
		})

		// 先頭から順番にfeatureIndexの値を確認し、値が変わったところで分割して不純度を取得する
		// ただし分割した結果、どちらにも最低1つはデータが入っている必要がある
		d := data[0].Features[featureIndex]
		for i, item := range data {
			if item.Features[featureIndex] == d {
				continue
			}

			left := data[:i]                     // 左のデータ
			leftImpurity := impurityFunc(left)   // 左の不純度
			right := data[i:]                    // 右のデータ
			rightImpurity := impurityFunc(right) // 右の不純度
			gain := currentImpurity - round((leftImpurity*float64(len(left))+rightImpurity*float64(len(right)))/float64(len(data)), -10)

			// 利得が大きいか、利得が同じで不純度の差が小さいときに更新する
			if gain > maxGain || (gain == maxGain && impurityDiff > math.Abs(leftImpurity-rightImpurity)) {
				maxGain = gain
				impurityDiff = math.Abs(leftImpurity - rightImpurity)
				opt = &DispatchOption{
					FeatureIndex: featureIndex,
					Pivot:        (item.Features[featureIndex] + d) / 2,
				}
			}
			d = item.Features[featureIndex] // 分割したところの値で更新
		}
	}

	if opt == nil {
		return nil, getDispatchOptionErrorUnable
	}

	return opt, nil
}

// itemを指定したoptでどこに振り分けられるかをチェック
func dispatch(item *DataItem, opt *DispatchOption) Side {
	if v, ok := item.Features[opt.FeatureIndex]; ok {
		if v < opt.Pivot {
			return LeftSide
		} else {
			return RightSide
		}
	}
	return OtherSide
}

// 任意の桁で四捨五入
func round(n float64, p int) float64 {
	p++
	return float64(int(n*math.Pow10(p*-1)+0.5)) * math.Pow10(p)
}
