package tree

import (
	"reflect"
	"testing"
)

func TestCategoryCount(t *testing.T) {
	testTable := []struct {
		data   []*DataItem
		expect map[string]int
	}{
		{[]*DataItem{}, map[string]int{}},
		{[]*DataItem{{Category: "0"}}, map[string]int{"0": 1}},
		{[]*DataItem{{Category: "1"}, {Category: "1"}}, map[string]int{"1": 2}},
		{[]*DataItem{{Category: "0"}, {Category: "1"}}, map[string]int{"0": 1, "1": 1}},
		{[]*DataItem{{Category: "10"}, {Category: "100"}}, map[string]int{"10": 1, "100": 1}},
	}

	for i, test := range testTable {
		actual := categoryCount(test.data)
		if !reflect.DeepEqual(test.expect, actual) {
			t.Fatalf("%s No.%02d 失敗\n期待: %+v\n実際: %+v\n", t.Name(), i+1, test.expect, actual)
		}
	}
}
