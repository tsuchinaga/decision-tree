package tree

import (
	"reflect"
	"testing"
)

func TestGetDispatchOption(t *testing.T) {
	testTable := []struct {
		data          []*DataItem
		impurityIndex ImpurityIndex
		expectOpt     *DispatchOption
		expectError   error
	}{
		{[]*DataItem{}, Gini, nil, getDispatchOptionErrorShortage},
		{[]*DataItem{{}}, Gini, nil, getDispatchOptionErrorShortage},
		{[]*DataItem{{}, {}}, Gini, nil, getDispatchOptionErrorMinimum},
		{[]*DataItem{{Features: map[string]float64{"0": 0}, Category: "0"}, {Features: map[string]float64{"0": 1}, Category: "1"}}, Gini, &DispatchOption{"0", 0.5}, nil},
		{[]*DataItem{{Features: map[string]float64{"0": 0, "1": 0}, Category: "0"}, {Features: map[string]float64{"0": 0, "1": 1}, Category: "1"}}, Gini, &DispatchOption{"1", 0.5}, nil},
		{[]*DataItem{{Features: map[string]float64{"2": 0}, Category: "0"}, {Features: map[string]float64{"2": 1}, Category: "1"}}, Gini, &DispatchOption{"2", 0.5}, nil},
		{[]*DataItem{{Features: map[string]float64{"0": 0}, Category: "0"}, {Features: map[string]float64{"0": 1}, Category: "1"}, {Features: map[string]float64{"0": 2}, Category: "2"}}, Gini, &DispatchOption{"0", 0.5}, nil},
		{[]*DataItem{
			{Features: map[string]float64{"0": 0}, Category: "0"},
			{Features: map[string]float64{"0": 1}, Category: "1"},
			{Features: map[string]float64{"0": 2}, Category: "2"},
			{Features: map[string]float64{"0": 3}, Category: "3"},
		}, Gini, &DispatchOption{"0", 1.5}, nil},
		{[]*DataItem{
			{Features: map[string]float64{"0": 0}, Category: "0"},
			{Features: map[string]float64{"0": 1}, Category: "1"},
			{Features: map[string]float64{"0": 2}, Category: "2"},
			{Features: map[string]float64{"0": 3}, Category: "3"},
			{Features: map[string]float64{"0": 4}, Category: "4"},
		}, Gini, &DispatchOption{"0", 1.5}, nil},
		{[]*DataItem{
			{Features: map[string]float64{"0": 0}, Category: "0"},
			{Features: map[string]float64{"0": 1}, Category: "1"},
			{Features: map[string]float64{"0": 2}, Category: "2"},
			{Features: map[string]float64{"0": 3}, Category: "3"},
			{Features: map[string]float64{"0": 4}, Category: "4"},
			{Features: map[string]float64{"0": 5}, Category: "5"},
		}, Gini, &DispatchOption{"0", 2.5}, nil},
	}
	for i, test := range testTable {
		actualOpt, actualError := getDispatchOption(test.data, test.impurityIndex)
		if !reflect.DeepEqual(test.expectOpt, actualOpt) {
			t.Fatalf("%s No.%02d 失敗(func)\n期待: %+v\n実際: %+v\n", t.Name(), i+1, test.expectOpt, actualOpt)
		}
		if test.expectError != actualError {
			t.Fatalf("%s No.%02d 失敗(error)\n期待: %+v\n実際: %+v\n", t.Name(), i+1, test.expectError, actualError)
		}
	}
}

func TestDispatch(t *testing.T) {
	testTable := []struct {
		item   *DataItem
		opt    *DispatchOption
		expect Side
	}{
		{&DataItem{Features: map[string]float64{"0": -1}}, &DispatchOption{"0", 1.0}, LeftSide},
		{&DataItem{Features: map[string]float64{"0": 0}}, &DispatchOption{"0", 1.0}, LeftSide},
		{&DataItem{Features: map[string]float64{"0": 0.9999}}, &DispatchOption{"0", 1.0}, LeftSide},
		{&DataItem{Features: map[string]float64{"0": 1}}, &DispatchOption{"0", 1.0}, RightSide},
		{&DataItem{Features: map[string]float64{"0": 2}}, &DispatchOption{"0", 1.0}, RightSide},
		{&DataItem{Features: map[string]float64{"1": 1}}, &DispatchOption{"0", 1.0}, OtherSide},
	}

	for i, test := range testTable {
		actual := dispatch(test.item, test.opt)
		if test.expect != actual {
			t.Fatalf("%s No.%02d 失敗\n期待: %+v, 実際: %+v\n", t.Name(), i+1, test.expect, actual)
		}
	}
}

func TestCreateNode(t *testing.T) {
	testTable := []struct {
		data          []*DataItem
		leftCategory  map[string]int
		rightCategory map[string]int
	}{
		// そもそも分けようがない
		{[]*DataItem{}, nil, nil},
		{[]*DataItem{{map[string]float64{"0": 0}, "0"}}, nil, nil},

		// 全て同じカテゴリ
		{[]*DataItem{{map[string]float64{"0": 0}, "0"}, {map[string]float64{"0": 1}, "0"}}, nil, nil},

		// 今ある基準では分けられない
		{[]*DataItem{{map[string]float64{"0": 0}, "0"}, {map[string]float64{"0": 0}, "1"}}, nil, nil},
		{[]*DataItem{{map[string]float64{"0": 0}, "0"}, {map[string]float64{"1": 1}, "1"}}, nil, nil},

		// 丁度半分に分けれる
		{[]*DataItem{{map[string]float64{"0": 0}, "0"}, {map[string]float64{"0": 1}, "1"}}, map[string]int{"0": 1}, map[string]int{"1": 1}},

		// 元の順番に関わらず分けられる
		{[]*DataItem{{map[string]float64{"0": 0}, "0"}, {map[string]float64{"0": 1}, "1"}, {map[string]float64{"0": 0}, "0"}}, map[string]int{"0": 2}, map[string]int{"1": 1}},

		// 右の方が不純度が高い
		{[]*DataItem{{map[string]float64{"0": 0}, "0"}, {map[string]float64{"0": 1}, "1"}, {map[string]float64{"0": 2}, "2"}}, map[string]int{"0": 1}, map[string]int{"1": 1, "2": 1}},

		// 左右で同じように切ったほうが不純度の差が小さい
		{[]*DataItem{{map[string]float64{"0": 0}, "0"}, {map[string]float64{"0": 1}, "1"}, {map[string]float64{"0": 2}, "2"}, {map[string]float64{"0": 3}, "3"}}, map[string]int{"0": 1, "1": 1}, map[string]int{"2": 1, "3": 1}},
	}

	for i, test := range testTable {
		actual := createNode(test.data, Gini, 1)
		if (test.leftCategory == nil && actual.LeftNode != nil) || (test.leftCategory != nil && actual.LeftNode == nil) {
			t.Fatalf("%s No.%02d(Left) 失敗\n期待: %+v\n実際: %+v\n", t.Name(), i+1, test.leftCategory, actual.LeftNode)
		}

		if test.leftCategory != nil && !reflect.DeepEqual(test.leftCategory, actual.LeftNode.CategoryCount) {
			t.Fatalf("%s No.%02d(Left) 失敗\n期待: %+v\n実際: %+v\n", t.Name(), i+1, test.leftCategory, actual.LeftNode.CategoryCount)
		}

		if (test.rightCategory == nil && actual.RightNode != nil) || (test.rightCategory != nil && actual.RightNode == nil) {
			t.Fatalf("%s No.%02d(Right) 失敗\n期待: %+v\n実際: %+v\n", t.Name(), i+1, test.rightCategory, actual.RightNode)
		}

		if test.rightCategory != nil && !reflect.DeepEqual(test.rightCategory, actual.RightNode.CategoryCount) {
			t.Fatalf("%s No.%02d(Right) 失敗\n期待: %+v\n実際: %+v\n", t.Name(), i+1, test.rightCategory, actual.RightNode.CategoryCount)
		}
	}
}

func TestCreateTree(t *testing.T) {
	impurityIndex := Gini
	pruner := NewPrunerOption(DepthPruner, 2)
	testTable := []struct {
		dataset                 Dataset
		expectFeatureNames      map[string]string
		expectCategoryNames     map[string]string
		expectNodeCategoryCount map[string]int
	}{
		{Dataset{map[string]string{"1": "A", "2": "B"}, map[string]string{"1": "YES", "2": "NO"}, []*DataItem{{Category: "1"}, {Category: "2"}}},
			map[string]string{"1": "A", "2": "B"},
			map[string]string{"1": "YES", "2": "NO"},
			map[string]int{"1": 1, "2": 1}},
		{Dataset{map[string]string{"1": "A"}, map[string]string{"1": "RESULT"}, []*DataItem{{Category: "0"}, {Category: "1"}}},
			map[string]string{"1": "A"},
			map[string]string{"1": "RESULT"},
			map[string]int{"0": 1, "1": 1}},
		{Dataset{map[string]string{"1": "A"}, map[string]string{"1": "RESULT"}, []*DataItem{{Category: "0"}, {Category: "0"}}},
			map[string]string{"1": "A"},
			map[string]string{"1": "RESULT"},
			map[string]int{"0": 2}},
	}

	for i, test := range testTable {
		actual := CreateTree(&test.dataset, impurityIndex, pruner)

		if !reflect.DeepEqual(test.expectFeatureNames, actual.FeatureNames) {
			t.Fatalf("%s No.%02d 失敗(FeatureNames)\n期待: %+v\n実際: %+v\n", t.Name(), i+1, test.expectFeatureNames, actual.FeatureNames)
		}
		if !reflect.DeepEqual(test.expectCategoryNames, actual.CategoryNames) {
			t.Fatalf("%s No.%02d 失敗(CategoryNames)\n期待: %+v\n実際: %+v\n", t.Name(), i+1, test.expectCategoryNames, actual.CategoryNames)
		}
		if !reflect.DeepEqual(test.expectNodeCategoryCount, actual.Root.CategoryCount) {
			t.Fatalf("%s No.%02d 失敗(CategoryCount)\n期待: %+v\n実際: %+v\n", t.Name(), i+1, test.expectNodeCategoryCount, actual.Root.CategoryCount)
		}
	}
}

func TestTree_Judge(t *testing.T) {
	dataset := Dataset{
		Data: []*DataItem{{map[string]float64{"1": 1}, "1"}, {map[string]float64{"1": 2}, "2"}, {map[string]float64{"1": 3}, "3"}, {map[string]float64{"1": 4}, "4"}},
	}
	tree := CreateTree(&dataset, Gini, NewPrunerOption(DepthPruner, 3))
	testTable := []struct {
		item   *DataItem
		expect *Node
	}{
		{&DataItem{map[string]float64{"1": -1}, "0"}, &Node{Depth: 3, IsLeaf: true, CategoryTotal: 1, CategoryCount: map[string]int{"1": 1}, CategoryRatio: map[string]float64{"1": 1}}},
		{&DataItem{map[string]float64{"1": 0}, "0"}, &Node{Depth: 3, IsLeaf: true, CategoryTotal: 1, CategoryCount: map[string]int{"1": 1}, CategoryRatio: map[string]float64{"1": 1}}},
		{&DataItem{map[string]float64{"1": 1}, "0"}, &Node{Depth: 3, IsLeaf: true, CategoryTotal: 1, CategoryCount: map[string]int{"1": 1}, CategoryRatio: map[string]float64{"1": 1}}},
		{&DataItem{map[string]float64{"1": 2}, "0"}, &Node{Depth: 3, IsLeaf: true, CategoryTotal: 1, CategoryCount: map[string]int{"2": 1}, CategoryRatio: map[string]float64{"2": 1}}},
		{&DataItem{map[string]float64{"1": 3}, "0"}, &Node{Depth: 3, IsLeaf: true, CategoryTotal: 1, CategoryCount: map[string]int{"3": 1}, CategoryRatio: map[string]float64{"3": 1}}},
		{&DataItem{map[string]float64{"1": 4}, "0"}, &Node{Depth: 3, IsLeaf: true, CategoryTotal: 1, CategoryCount: map[string]int{"4": 1}, CategoryRatio: map[string]float64{"4": 1}}},
		{&DataItem{map[string]float64{"1": 5}, "0"}, &Node{Depth: 3, IsLeaf: true, CategoryTotal: 1, CategoryCount: map[string]int{"4": 1}, CategoryRatio: map[string]float64{"4": 1}}},
	}

	for i, test := range testTable {
		actual := tree.Judge(test.item)
		if !reflect.DeepEqual(test.expect, actual) {
			t.Fatalf("%s No.%02d 失敗\n期待: %+v\n実際: %+v\n", t.Name(), i+1, test.expect, actual)
		}
	}
}

func TestSide_String(t *testing.T) {
	testTable := []struct {
		side   Side
		expect string
	}{
		{OtherSide, "OtherSide"},
		{LeftSide, "LeftSide"},
		{RightSide, "RightSide"},
	}

	for i, test := range testTable {
		actual := test.side.String()
		if test.expect != actual {
			t.Fatalf("%s No.%02d 失敗\n期待: %s, 実際: %s\n", t.Name(), i+1, test.expect, actual)
		}
	}
}

func TestTree_String(t *testing.T) {
	dataset := Dataset{map[string]string{"1": "A", "2": "B"}, map[string]string{"1": "YES", "2": "NO"}, []*DataItem{{map[string]float64{"0": 0}, "0"}, {map[string]float64{"0": 1}, "1"}, {map[string]float64{"0": 2}, "2"}}}
	impurityIndex := Gini
	actual := CreateTree(&dataset, impurityIndex, NewPrunerOption(DepthPruner, 3)).String()
	expect := `Tree {
	FeatureNames: map[1:A 2:B]
	CategoryNames: map[1:YES 2:NO]
	ImpurityIndex: Gini
	Pruner: PrunerOption{Pruner: DepthPruner, Reference: 3.000000}
	Root: {
		Depth: 1
		IsLeaf: false
		CategoryTotal: 3
		CategoryCount: map[0:1 1:1 2:1]
		CategoryRatio: map[0:0.3333333333333333 1:0.3333333333333333 2:0.3333333333333333]
		Impurity: 0.666667
		DispatchOption: {FeatureIndex: 0, Pivot: 0.500000}
		LeftNode: {
			Depth: 2
			IsLeaf: true
			CategoryTotal: 1
			CategoryCount: map[0:1]
			CategoryRatio: map[0:1]
			Impurity: 0.000000
		}
		RightNode: {
			Depth: 2
			IsLeaf: false
			CategoryTotal: 2
			CategoryCount: map[1:1 2:1]
			CategoryRatio: map[1:0.5 2:0.5]
			Impurity: 0.500000
			DispatchOption: {FeatureIndex: 0, Pivot: 1.500000}
			LeftNode: {
				Depth: 3
				IsLeaf: true
				CategoryTotal: 1
				CategoryCount: map[1:1]
				CategoryRatio: map[1:1]
				Impurity: 0.000000
			}
			RightNode: {
				Depth: 3
				IsLeaf: true
				CategoryTotal: 1
				CategoryCount: map[2:1]
				CategoryRatio: map[2:1]
				Impurity: 0.000000
			}
		}
	}
}
`

	if expect != actual {
		t.Fatalf("%s 失敗\n期待: %s\n実際: %s\n", t.Name(), expect, actual)
	}
}

func TestNode_CountLeaf(t *testing.T) {
	testTable := []struct {
		node   *Node
		expect int
	}{
		{&Node{IsLeaf: true}, 1},                                                  // 最初から末端
		{&Node{LeftNode: &Node{LeftNode: &Node{IsLeaf: true}}}, 1},                // 左側にまっすぐ
		{&Node{RightNode: &Node{RightNode: &Node{IsLeaf: true}}}, 1},              // 右側にまっすぐ
		{&Node{LeftNode: &Node{IsLeaf: true}, RightNode: &Node{IsLeaf: true}}, 2}, // 左右に一つずつ
		{&Node{LeftNode: &Node{LeftNode: &Node{IsLeaf: true}, RightNode: &Node{IsLeaf: true}}, RightNode: &Node{LeftNode: &Node{IsLeaf: true}, RightNode: &Node{IsLeaf: true}}}, 4}, // 左右に2つずつ
	}

	for i, test := range testTable {
		actual := test.node.CountLeaf()
		if test.expect != actual {
			t.Fatalf("%s 失敗 No.%02d\n期待: %d, 実際: %d\n", t.Name(), i+1, test.expect, actual)
		}
	}
}

func TestRound(t *testing.T) {
	testTable := []struct {
		n      float64
		p      int
		expect float64
	}{
		{0, -2, 0},
		{0.4, -2, 0.4},
		{0.04, -2, 0},
		{0.05, -2, 0.1},
		{0, -1, 0},
		{4, -1, 4},
		{0.4, -1, 0},
		{0.5, -1, 1},
		{1, -1, 1},
		{1.5, -1, 2},
		{0, 0, 0},
		{4, 0, 0},
		{5, 0, 10},
		{10, 0, 10},
		{14, 0, 10},
		{49, 1, 0},
		{50, 1, 100},
		{149, 1, 100},
		{150, 1, 200},
	}

	for i, test := range testTable {
		actual := round(test.n, test.p)
		if test.expect != actual {
			t.Fatalf("%s No.%02d 失敗\n期待: %f, 実際: %f", t.Name(), i+1, test.expect, actual)
		}
	}
}

func TestCreateTree2(t *testing.T) {
	// irisテスト
	// log.Println(CreateTree(&dataset1, 3, Gini))
}
