package tree

import (
	"math"
)

// 不純度の指標
type ImpurityIndex int

const (
	Gini    ImpurityIndex = iota // ジニ不純度
	Entropy                      // エントロピー
)

// stringer
func (f ImpurityIndex) String() string {
	switch f {
	case Gini:
		return "Gini"
	case Entropy:
		return "Entropy"
	}
	return "Unknown ImpurityIndex"
}

// 指定した不純度計算方法の関数を取り出す
func (f ImpurityIndex) GetFunction() func([]*DataItem) float64 {
	switch f {
	case Gini:
		return gini
	case Entropy:
		return entropy
	}
	return nil
}

// ジニ不純度
func gini(data []*DataItem) float64 {
	categoryCount := categoryCount(data)
	total := float64(len(data))
	sum := .0
	for _, cnt := range categoryCount {
		sum += math.Pow(float64(cnt)/total, 2)
	}
	return 1 - sum
}

// エントロピー
func entropy(data []*DataItem) float64 {
	categoryCount := categoryCount(data)
	total := float64(len(data))
	sum := .0
	for _, cnt := range categoryCount {
		if cnt > 0 {
			per := float64(cnt) / total
			sum += per * (math.Log(per) / math.Log(total))
		}
	}
	return sum * -1
}
