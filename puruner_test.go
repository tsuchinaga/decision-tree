package tree

import (
	"math"
	"reflect"
	"testing"
)

func TestPruner_String(t *testing.T) {
	testTable := []struct {
		p      Pruner
		expect string
	}{
		{DepthPruner, "DepthPruner"},
		{ImpurityPruner, "ImpurityPruner"},
	}

	for i, test := range testTable {
		actual := test.p.String()
		if test.expect != actual {
			t.Fatalf("%s No.%02d 失敗\n期待: %s, 実際: %s\n", t.Name(), i+1, test.expect, actual)
		}
	}
}

func TestPruner_GetFunction(t *testing.T) {
	testTable := []struct {
		p          Pruner
		expectFunc func(*Tree, float64) error
		ref        float64
	}{
		{DepthPruner, depthPruner, -1},
		{DepthPruner, depthPruner, 0},
		{DepthPruner, depthPruner, 0.999},
		{DepthPruner, depthPruner, 1},
		{DepthPruner, depthPruner, 2},
		{DepthPruner, depthPruner, 3},
		{ImpurityPruner, impurityPruner, -1},
		{ImpurityPruner, impurityPruner, 0},
		{ImpurityPruner, impurityPruner, 0.49},
		{ImpurityPruner, impurityPruner, 0.5},
		{ImpurityPruner, impurityPruner, 0.6},
		{ImpurityPruner, impurityPruner, 0.7},
		{ImpurityPruner, impurityPruner, 0.99},
		{ImpurityPruner, impurityPruner, 1},
	}

	for i, test := range testTable {
		tree1, tree2 := testTree()
		actualFunc := test.p.GetFunction()
		expect := test.expectFunc(tree1, test.ref)
		actual := actualFunc(tree2, test.ref)
		if expect != actual {
			t.Fatalf("%s No.%02d 失敗\n期待: %+v\n実際: %+v\n", t.Name(), i+1, expect, actual)
		}
		if !reflect.DeepEqual(tree1, tree2) {
			t.Fatalf("%s No.%02d 失敗\n期待: %+v\n実際: %+v\n", t.Name(), i+1, tree1, tree2)
		}
	}
}

func TestDepthCut(t *testing.T) {
	testTable := []struct {
		ref    float64
		expect int
	}{
		{-1, 6},
		{0, 6},
		{0.999, 6},
		{1, 1},
		{2, 2},
		{3, 3},
	}

	for i, test := range testTable {
		tree1, _ := testTree()
		depthCut(tree1.Root, 1, test.ref)
		actual := checkDepth(tree1.Root)

		if test.expect != actual {
			t.Fatalf("%s No.%02d 失敗\n期待: %+v\n実際: %+v\n", t.Name(), i+1, test.expect, actual)
		}
	}
}

func TestImpurityCut(t *testing.T) {
	testTable := []struct {
		ref    float64
		expect int
	}{
		{-1, 6},
		{0, 6},
		{0.49, 3},
		{0.5, 3},
		{0.51, 2},
		{0.6, 2},
		{0.7, 1},
		{1, 1},
	}

	for i, test := range testTable {
		tree1, _ := testTree()
		impurityCut(tree1.Root, test.ref)
		actual := checkDepth(tree1.Root)

		if test.expect != actual {
			t.Fatalf("%s No.%02d 失敗\n期待: %+v\n実際: %+v\n", t.Name(), i+1, test.expect, actual)
		}
	}
}

func testTree() (*Tree, *Tree) {
	tree1 := &Tree{
		Root: &Node{
			CategoryCount:  map[string]int{"1": 50, "2": 50, "3": 50},
			Impurity:       0.666667,
			DispatchOption: &DispatchOption{},
			LeftNode:       &Node{IsLeaf: true, CategoryCount: map[string]int{"1": 50}},
			RightNode: &Node{
				CategoryCount:  map[string]int{"2": 50, "3": 50},
				Impurity:       0.500000,
				DispatchOption: &DispatchOption{},
				LeftNode: &Node{
					CategoryCount:  map[string]int{"2": 49, "3": 5},
					Impurity:       0.168038,
					DispatchOption: &DispatchOption{},
					LeftNode: &Node{
						CategoryCount:  map[string]int{"2": 47, "3": 1},
						Impurity:       0.040799,
						DispatchOption: &DispatchOption{},
						LeftNode:       &Node{IsLeaf: true, CategoryCount: map[string]int{"2": 47}},
						RightNode:      &Node{IsLeaf: true, CategoryCount: map[string]int{"3": 1}},
					},
					RightNode: &Node{
						CategoryCount:  map[string]int{"2": 2, "3": 4},
						Impurity:       0.444444,
						DispatchOption: &DispatchOption{},
						LeftNode:       &Node{IsLeaf: true, CategoryCount: map[string]int{"3": 3}},
						RightNode: &Node{
							IsLeaf:        true,
							CategoryCount: map[string]int{"2": 2, "3": 1},
							Impurity:      0.444444,
							LeftNode:      &Node{IsLeaf: true, CategoryCount: map[string]int{"2": 2}},
							RightNode:     &Node{IsLeaf: true, CategoryCount: map[string]int{"3": 1}},
						},
					},
				},
				RightNode: &Node{
					CategoryCount:  map[string]int{"2": 1, "3": 45},
					Impurity:       0.042533,
					DispatchOption: &DispatchOption{},
					LeftNode: &Node{
						CategoryCount:  map[string]int{"2": 1, "3": 2},
						Impurity:       0.444444,
						DispatchOption: &DispatchOption{},
						LeftNode:       &Node{IsLeaf: true, CategoryCount: map[string]int{"2": 1}},
						RightNode:      &Node{IsLeaf: true, CategoryCount: map[string]int{"3": 2}},
					},
					RightNode: &Node{IsLeaf: true, CategoryCount: map[string]int{"3": 43}},
				},
			},
		},
	}
	tree2 := &Tree{
		Root: &Node{
			CategoryCount:  map[string]int{"1": 50, "2": 50, "3": 50},
			Impurity:       0.666667,
			DispatchOption: &DispatchOption{},
			LeftNode: &Node{
				IsLeaf:        true,
				CategoryCount: map[string]int{"1": 50},
				Impurity:      0.000000,
			},
			RightNode: &Node{
				CategoryCount:  map[string]int{"2": 50, "3": 50},
				Impurity:       0.500000,
				DispatchOption: &DispatchOption{},
				LeftNode: &Node{
					CategoryCount:  map[string]int{"2": 49, "3": 5},
					Impurity:       0.168038,
					DispatchOption: &DispatchOption{},
					LeftNode: &Node{
						CategoryCount:  map[string]int{"2": 47, "3": 1},
						Impurity:       0.040799,
						DispatchOption: &DispatchOption{},
						LeftNode:       &Node{IsLeaf: true, CategoryCount: map[string]int{"2": 47}},
						RightNode:      &Node{IsLeaf: true, CategoryCount: map[string]int{"3": 1}},
					},
					RightNode: &Node{
						CategoryCount:  map[string]int{"2": 2, "3": 4},
						Impurity:       0.444444,
						DispatchOption: &DispatchOption{},
						LeftNode:       &Node{IsLeaf: true, CategoryCount: map[string]int{"3": 3}},
						RightNode: &Node{
							IsLeaf:        true,
							CategoryCount: map[string]int{"2": 2, "3": 1},
							Impurity:      0.444444,
							LeftNode:      &Node{IsLeaf: true, CategoryCount: map[string]int{"2": 2}},
							RightNode:     &Node{IsLeaf: true, CategoryCount: map[string]int{"3": 1}},
						},
					},
				},
				RightNode: &Node{
					CategoryCount:  map[string]int{"2": 1, "3": 45},
					Impurity:       0.042533,
					DispatchOption: &DispatchOption{},
					LeftNode: &Node{
						CategoryCount:  map[string]int{"2": 1, "3": 2},
						Impurity:       0.444444,
						DispatchOption: &DispatchOption{},
						LeftNode:       &Node{IsLeaf: true, CategoryCount: map[string]int{"2": 1}},
						RightNode:      &Node{IsLeaf: true, CategoryCount: map[string]int{"3": 2}},
					},
					RightNode: &Node{IsLeaf: true, CategoryCount: map[string]int{"3": 43}},
				},
			},
		},
	}
	return tree1, tree2
}

func checkDepth(node *Node) int {
	var ld, rd int
	if node.LeftNode != nil {
		ld = checkDepth(node.LeftNode)
	}
	if node.RightNode != nil {
		rd = checkDepth(node.RightNode)
	}
	return int(math.Max(float64(ld), float64(rd))) + 1
}
