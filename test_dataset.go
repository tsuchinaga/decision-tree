package tree

// https://gist.githubusercontent.com/curran/a08a1080b88344b0c8a7/raw/d546eaee765268bf2f487608c537c05e22e4b221/iris.csv
var dataset1 = Dataset{
	CategoryNames: map[string]string{"1": "setosa", "2": "versicolor", "3": "virginica"},
	FeatureNames:  map[string]string{"1": "sepal_length", "2": "sepal_width", "3": "petal_length", "4": "petal_width"},
	Data: []*DataItem{
		{Features: map[string]float64{"1": 5.1, "2": 3.5, "3": 1.4, "4": 0.2}, Category: "1"},
		{Features: map[string]float64{"1": 4.9, "2": 3, "3": 1.4, "4": 0.2}, Category: "1"},
		{Features: map[string]float64{"1": 4.7, "2": 3.2, "3": 1.3, "4": 0.2}, Category: "1"},
		{Features: map[string]float64{"1": 4.6, "2": 3.1, "3": 1.5, "4": 0.2}, Category: "1"},
		{Features: map[string]float64{"1": 5, "2": 3.6, "3": 1.4, "4": 0.2}, Category: "1"},
		{Features: map[string]float64{"1": 5.4, "2": 3.9, "3": 1.7, "4": 0.4}, Category: "1"},
		{Features: map[string]float64{"1": 4.6, "2": 3.4, "3": 1.4, "4": 0.3}, Category: "1"},
		{Features: map[string]float64{"1": 5, "2": 3.4, "3": 1.5, "4": 0.2}, Category: "1"},
		{Features: map[string]float64{"1": 4.4, "2": 2.9, "3": 1.4, "4": 0.2}, Category: "1"},
		{Features: map[string]float64{"1": 4.9, "2": 3.1, "3": 1.5, "4": 0.1}, Category: "1"},
		{Features: map[string]float64{"1": 5.4, "2": 3.7, "3": 1.5, "4": 0.2}, Category: "1"},
		{Features: map[string]float64{"1": 4.8, "2": 3.4, "3": 1.6, "4": 0.2}, Category: "1"},
		{Features: map[string]float64{"1": 4.8, "2": 3, "3": 1.4, "4": 0.1}, Category: "1"},
		{Features: map[string]float64{"1": 4.3, "2": 3, "3": 1.1, "4": 0.1}, Category: "1"},
		{Features: map[string]float64{"1": 5.8, "2": 4, "3": 1.2, "4": 0.2}, Category: "1"},
		{Features: map[string]float64{"1": 5.7, "2": 4.4, "3": 1.5, "4": 0.4}, Category: "1"},
		{Features: map[string]float64{"1": 5.4, "2": 3.9, "3": 1.3, "4": 0.4}, Category: "1"},
		{Features: map[string]float64{"1": 5.1, "2": 3.5, "3": 1.4, "4": 0.3}, Category: "1"},
		{Features: map[string]float64{"1": 5.7, "2": 3.8, "3": 1.7, "4": 0.3}, Category: "1"},
		{Features: map[string]float64{"1": 5.1, "2": 3.8, "3": 1.5, "4": 0.3}, Category: "1"},
		{Features: map[string]float64{"1": 5.4, "2": 3.4, "3": 1.7, "4": 0.2}, Category: "1"},
		{Features: map[string]float64{"1": 5.1, "2": 3.7, "3": 1.5, "4": 0.4}, Category: "1"},
		{Features: map[string]float64{"1": 4.6, "2": 3.6, "3": 1, "4": 0.2}, Category: "1"},
		{Features: map[string]float64{"1": 5.1, "2": 3.3, "3": 1.7, "4": 0.5}, Category: "1"},
		{Features: map[string]float64{"1": 4.8, "2": 3.4, "3": 1.9, "4": 0.2}, Category: "1"},
		{Features: map[string]float64{"1": 5, "2": 3, "3": 1.6, "4": 0.2}, Category: "1"},
		{Features: map[string]float64{"1": 5, "2": 3.4, "3": 1.6, "4": 0.4}, Category: "1"},
		{Features: map[string]float64{"1": 5.2, "2": 3.5, "3": 1.5, "4": 0.2}, Category: "1"},
		{Features: map[string]float64{"1": 5.2, "2": 3.4, "3": 1.4, "4": 0.2}, Category: "1"},
		{Features: map[string]float64{"1": 4.7, "2": 3.2, "3": 1.6, "4": 0.2}, Category: "1"},
		{Features: map[string]float64{"1": 4.8, "2": 3.1, "3": 1.6, "4": 0.2}, Category: "1"},
		{Features: map[string]float64{"1": 5.4, "2": 3.4, "3": 1.5, "4": 0.4}, Category: "1"},
		{Features: map[string]float64{"1": 5.2, "2": 4.1, "3": 1.5, "4": 0.1}, Category: "1"},
		{Features: map[string]float64{"1": 5.5, "2": 4.2, "3": 1.4, "4": 0.2}, Category: "1"},
		{Features: map[string]float64{"1": 4.9, "2": 3.1, "3": 1.5, "4": 0.1}, Category: "1"},
		{Features: map[string]float64{"1": 5, "2": 3.2, "3": 1.2, "4": 0.2}, Category: "1"},
		{Features: map[string]float64{"1": 5.5, "2": 3.5, "3": 1.3, "4": 0.2}, Category: "1"},
		{Features: map[string]float64{"1": 4.9, "2": 3.1, "3": 1.5, "4": 0.1}, Category: "1"},
		{Features: map[string]float64{"1": 4.4, "2": 3, "3": 1.3, "4": 0.2}, Category: "1"},
		{Features: map[string]float64{"1": 5.1, "2": 3.4, "3": 1.5, "4": 0.2}, Category: "1"},
		{Features: map[string]float64{"1": 5, "2": 3.5, "3": 1.3, "4": 0.3}, Category: "1"},
		{Features: map[string]float64{"1": 4.5, "2": 2.3, "3": 1.3, "4": 0.3}, Category: "1"},
		{Features: map[string]float64{"1": 4.4, "2": 3.2, "3": 1.3, "4": 0.2}, Category: "1"},
		{Features: map[string]float64{"1": 5, "2": 3.5, "3": 1.6, "4": 0.6}, Category: "1"},
		{Features: map[string]float64{"1": 5.1, "2": 3.8, "3": 1.9, "4": 0.4}, Category: "1"},
		{Features: map[string]float64{"1": 4.8, "2": 3, "3": 1.4, "4": 0.3}, Category: "1"},
		{Features: map[string]float64{"1": 5.1, "2": 3.8, "3": 1.6, "4": 0.2}, Category: "1"},
		{Features: map[string]float64{"1": 4.6, "2": 3.2, "3": 1.4, "4": 0.2}, Category: "1"},
		{Features: map[string]float64{"1": 5.3, "2": 3.7, "3": 1.5, "4": 0.2}, Category: "1"},
		{Features: map[string]float64{"1": 5, "2": 3.3, "3": 1.4, "4": 0.2}, Category: "1"},
		{Features: map[string]float64{"1": 7, "2": 3.2, "3": 4.7, "4": 1.4}, Category: "2"},
		{Features: map[string]float64{"1": 6.4, "2": 3.2, "3": 4.5, "4": 1.5}, Category: "2"},
		{Features: map[string]float64{"1": 6.9, "2": 3.1, "3": 4.9, "4": 1.5}, Category: "2"},
		{Features: map[string]float64{"1": 5.5, "2": 2.3, "3": 4, "4": 1.3}, Category: "2"},
		{Features: map[string]float64{"1": 6.5, "2": 2.8, "3": 4.6, "4": 1.5}, Category: "2"},
		{Features: map[string]float64{"1": 5.7, "2": 2.8, "3": 4.5, "4": 1.3}, Category: "2"},
		{Features: map[string]float64{"1": 6.3, "2": 3.3, "3": 4.7, "4": 1.6}, Category: "2"},
		{Features: map[string]float64{"1": 4.9, "2": 2.4, "3": 3.3, "4": 1}, Category: "2"},
		{Features: map[string]float64{"1": 6.6, "2": 2.9, "3": 4.6, "4": 1.3}, Category: "2"},
		{Features: map[string]float64{"1": 5.2, "2": 2.7, "3": 3.9, "4": 1.4}, Category: "2"},
		{Features: map[string]float64{"1": 5, "2": 2, "3": 3.5, "4": 1}, Category: "2"},
		{Features: map[string]float64{"1": 5.9, "2": 3, "3": 4.2, "4": 1.5}, Category: "2"},
		{Features: map[string]float64{"1": 6, "2": 2.2, "3": 4, "4": 1}, Category: "2"},
		{Features: map[string]float64{"1": 6.1, "2": 2.9, "3": 4.7, "4": 1.4}, Category: "2"},
		{Features: map[string]float64{"1": 5.6, "2": 2.9, "3": 3.6, "4": 1.3}, Category: "2"},
		{Features: map[string]float64{"1": 6.7, "2": 3.1, "3": 4.4, "4": 1.4}, Category: "2"},
		{Features: map[string]float64{"1": 5.6, "2": 3, "3": 4.5, "4": 1.5}, Category: "2"},
		{Features: map[string]float64{"1": 5.8, "2": 2.7, "3": 4.1, "4": 1}, Category: "2"},
		{Features: map[string]float64{"1": 6.2, "2": 2.2, "3": 4.5, "4": 1.5}, Category: "2"},
		{Features: map[string]float64{"1": 5.6, "2": 2.5, "3": 3.9, "4": 1.1}, Category: "2"},
		{Features: map[string]float64{"1": 5.9, "2": 3.2, "3": 4.8, "4": 1.8}, Category: "2"},
		{Features: map[string]float64{"1": 6.1, "2": 2.8, "3": 4, "4": 1.3}, Category: "2"},
		{Features: map[string]float64{"1": 6.3, "2": 2.5, "3": 4.9, "4": 1.5}, Category: "2"},
		{Features: map[string]float64{"1": 6.1, "2": 2.8, "3": 4.7, "4": 1.2}, Category: "2"},
		{Features: map[string]float64{"1": 6.4, "2": 2.9, "3": 4.3, "4": 1.3}, Category: "2"},
		{Features: map[string]float64{"1": 6.6, "2": 3, "3": 4.4, "4": 1.4}, Category: "2"},
		{Features: map[string]float64{"1": 6.8, "2": 2.8, "3": 4.8, "4": 1.4}, Category: "2"},
		{Features: map[string]float64{"1": 6.7, "2": 3, "3": 5, "4": 1.7}, Category: "2"},
		{Features: map[string]float64{"1": 6, "2": 2.9, "3": 4.5, "4": 1.5}, Category: "2"},
		{Features: map[string]float64{"1": 5.7, "2": 2.6, "3": 3.5, "4": 1}, Category: "2"},
		{Features: map[string]float64{"1": 5.5, "2": 2.4, "3": 3.8, "4": 1.1}, Category: "2"},
		{Features: map[string]float64{"1": 5.5, "2": 2.4, "3": 3.7, "4": 1}, Category: "2"},
		{Features: map[string]float64{"1": 5.8, "2": 2.7, "3": 3.9, "4": 1.2}, Category: "2"},
		{Features: map[string]float64{"1": 6, "2": 2.7, "3": 5.1, "4": 1.6}, Category: "2"},
		{Features: map[string]float64{"1": 5.4, "2": 3, "3": 4.5, "4": 1.5}, Category: "2"},
		{Features: map[string]float64{"1": 6, "2": 3.4, "3": 4.5, "4": 1.6}, Category: "2"},
		{Features: map[string]float64{"1": 6.7, "2": 3.1, "3": 4.7, "4": 1.5}, Category: "2"},
		{Features: map[string]float64{"1": 6.3, "2": 2.3, "3": 4.4, "4": 1.3}, Category: "2"},
		{Features: map[string]float64{"1": 5.6, "2": 3, "3": 4.1, "4": 1.3}, Category: "2"},
		{Features: map[string]float64{"1": 5.5, "2": 2.5, "3": 4, "4": 1.3}, Category: "2"},
		{Features: map[string]float64{"1": 5.5, "2": 2.6, "3": 4.4, "4": 1.2}, Category: "2"},
		{Features: map[string]float64{"1": 6.1, "2": 3, "3": 4.6, "4": 1.4}, Category: "2"},
		{Features: map[string]float64{"1": 5.8, "2": 2.6, "3": 4, "4": 1.2}, Category: "2"},
		{Features: map[string]float64{"1": 5, "2": 2.3, "3": 3.3, "4": 1}, Category: "2"},
		{Features: map[string]float64{"1": 5.6, "2": 2.7, "3": 4.2, "4": 1.3}, Category: "2"},
		{Features: map[string]float64{"1": 5.7, "2": 3, "3": 4.2, "4": 1.2}, Category: "2"},
		{Features: map[string]float64{"1": 5.7, "2": 2.9, "3": 4.2, "4": 1.3}, Category: "2"},
		{Features: map[string]float64{"1": 6.2, "2": 2.9, "3": 4.3, "4": 1.3}, Category: "2"},
		{Features: map[string]float64{"1": 5.1, "2": 2.5, "3": 3, "4": 1.1}, Category: "2"},
		{Features: map[string]float64{"1": 5.7, "2": 2.8, "3": 4.1, "4": 1.3}, Category: "2"},
		{Features: map[string]float64{"1": 6.3, "2": 3.3, "3": 6, "4": 2.5}, Category: "3"},
		{Features: map[string]float64{"1": 5.8, "2": 2.7, "3": 5.1, "4": 1.9}, Category: "3"},
		{Features: map[string]float64{"1": 7.1, "2": 3, "3": 5.9, "4": 2.1}, Category: "3"},
		{Features: map[string]float64{"1": 6.3, "2": 2.9, "3": 5.6, "4": 1.8}, Category: "3"},
		{Features: map[string]float64{"1": 6.5, "2": 3, "3": 5.8, "4": 2.2}, Category: "3"},
		{Features: map[string]float64{"1": 7.6, "2": 3, "3": 6.6, "4": 2.1}, Category: "3"},
		{Features: map[string]float64{"1": 4.9, "2": 2.5, "3": 4.5, "4": 1.7}, Category: "3"},
		{Features: map[string]float64{"1": 7.3, "2": 2.9, "3": 6.3, "4": 1.8}, Category: "3"},
		{Features: map[string]float64{"1": 6.7, "2": 2.5, "3": 5.8, "4": 1.8}, Category: "3"},
		{Features: map[string]float64{"1": 7.2, "2": 3.6, "3": 6.1, "4": 2.5}, Category: "3"},
		{Features: map[string]float64{"1": 6.5, "2": 3.2, "3": 5.1, "4": 2}, Category: "3"},
		{Features: map[string]float64{"1": 6.4, "2": 2.7, "3": 5.3, "4": 1.9}, Category: "3"},
		{Features: map[string]float64{"1": 6.8, "2": 3, "3": 5.5, "4": 2.1}, Category: "3"},
		{Features: map[string]float64{"1": 5.7, "2": 2.5, "3": 5, "4": 2}, Category: "3"},
		{Features: map[string]float64{"1": 5.8, "2": 2.8, "3": 5.1, "4": 2.4}, Category: "3"},
		{Features: map[string]float64{"1": 6.4, "2": 3.2, "3": 5.3, "4": 2.3}, Category: "3"},
		{Features: map[string]float64{"1": 6.5, "2": 3, "3": 5.5, "4": 1.8}, Category: "3"},
		{Features: map[string]float64{"1": 7.7, "2": 3.8, "3": 6.7, "4": 2.2}, Category: "3"},
		{Features: map[string]float64{"1": 7.7, "2": 2.6, "3": 6.9, "4": 2.3}, Category: "3"},
		{Features: map[string]float64{"1": 6, "2": 2.2, "3": 5, "4": 1.5}, Category: "3"},
		{Features: map[string]float64{"1": 6.9, "2": 3.2, "3": 5.7, "4": 2.3}, Category: "3"},
		{Features: map[string]float64{"1": 5.6, "2": 2.8, "3": 4.9, "4": 2}, Category: "3"},
		{Features: map[string]float64{"1": 7.7, "2": 2.8, "3": 6.7, "4": 2}, Category: "3"},
		{Features: map[string]float64{"1": 6.3, "2": 2.7, "3": 4.9, "4": 1.8}, Category: "3"},
		{Features: map[string]float64{"1": 6.7, "2": 3.3, "3": 5.7, "4": 2.1}, Category: "3"},
		{Features: map[string]float64{"1": 7.2, "2": 3.2, "3": 6, "4": 1.8}, Category: "3"},
		{Features: map[string]float64{"1": 6.2, "2": 2.8, "3": 4.8, "4": 1.8}, Category: "3"},
		{Features: map[string]float64{"1": 6.1, "2": 3, "3": 4.9, "4": 1.8}, Category: "3"},
		{Features: map[string]float64{"1": 6.4, "2": 2.8, "3": 5.6, "4": 2.1}, Category: "3"},
		{Features: map[string]float64{"1": 7.2, "2": 3, "3": 5.8, "4": 1.6}, Category: "3"},
		{Features: map[string]float64{"1": 7.4, "2": 2.8, "3": 6.1, "4": 1.9}, Category: "3"},
		{Features: map[string]float64{"1": 7.9, "2": 3.8, "3": 6.4, "4": 2}, Category: "3"},
		{Features: map[string]float64{"1": 6.4, "2": 2.8, "3": 5.6, "4": 2.2}, Category: "3"},
		{Features: map[string]float64{"1": 6.3, "2": 2.8, "3": 5.1, "4": 1.5}, Category: "3"},
		{Features: map[string]float64{"1": 6.1, "2": 2.6, "3": 5.6, "4": 1.4}, Category: "3"},
		{Features: map[string]float64{"1": 7.7, "2": 3, "3": 6.1, "4": 2.3}, Category: "3"},
		{Features: map[string]float64{"1": 6.3, "2": 3.4, "3": 5.6, "4": 2.4}, Category: "3"},
		{Features: map[string]float64{"1": 6.4, "2": 3.1, "3": 5.5, "4": 1.8}, Category: "3"},
		{Features: map[string]float64{"1": 6, "2": 3, "3": 4.8, "4": 1.8}, Category: "3"},
		{Features: map[string]float64{"1": 6.9, "2": 3.1, "3": 5.4, "4": 2.1}, Category: "3"},
		{Features: map[string]float64{"1": 6.7, "2": 3.1, "3": 5.6, "4": 2.4}, Category: "3"},
		{Features: map[string]float64{"1": 6.9, "2": 3.1, "3": 5.1, "4": 2.3}, Category: "3"},
		{Features: map[string]float64{"1": 5.8, "2": 2.7, "3": 5.1, "4": 1.9}, Category: "3"},
		{Features: map[string]float64{"1": 6.8, "2": 3.2, "3": 5.9, "4": 2.3}, Category: "3"},
		{Features: map[string]float64{"1": 6.7, "2": 3.3, "3": 5.7, "4": 2.5}, Category: "3"},
		{Features: map[string]float64{"1": 6.7, "2": 3, "3": 5.2, "4": 2.3}, Category: "3"},
		{Features: map[string]float64{"1": 6.3, "2": 2.5, "3": 5, "4": 1.9}, Category: "3"},
		{Features: map[string]float64{"1": 6.5, "2": 3, "3": 5.2, "4": 2}, Category: "3"},
		{Features: map[string]float64{"1": 6.2, "2": 3.4, "3": 5.4, "4": 2.3}, Category: "3"},
		{Features: map[string]float64{"1": 5.9, "2": 3, "3": 5.1, "4": 1.8}, Category: "3"},
	},
}
