package tree

type Dataset struct {
	FeatureNames  map[string]string // 特徴の名前
	CategoryNames map[string]string // 分類の名前
	Data          []*DataItem       // データ
}

type DataItem struct {
	Features map[string]float64 // 特徴量
	Category string             // 分類
}

// dataに含まれるカテゴリの件数
func categoryCount(data []*DataItem) map[string]int {
	categoryCount := make(map[string]int)
	for _, item := range data {
		categoryCount[item.Category]++
	}
	return categoryCount
}
